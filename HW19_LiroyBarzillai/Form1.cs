﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HW19_LiroyBarzillai
{
    public partial class Form1 : Form
    {
        private string _userName;
        private string _password;

        public Form1()
        {
            InitializeComponent();
        }

        public string Username
        {
            get { return _userName; }
            set { _userName = value;}
        }

        public string Password
        {
            get { return _password; }
            set { _password = value;}
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            string[] lines = null;
            string[] usernameAndPassword = null;
            bool flag = false;

            try  //Checks if we can open the file
            {
                lines = System.IO.File.ReadAllLines(@"Users.txt");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            _userName = txtBoxUsername.Text;
            _password = txtBoxPassword.Text;

            for (int i = 0; i < lines.Length; i++)  //Checks the data which we got from the file
            {
                usernameAndPassword = lines[i].Split(',');
                if(usernameAndPassword[0] == _userName && usernameAndPassword[1] == _password)  //Checks if the user entered correct data
                {
                    flag = true;
                    break;
                }
            }
            if (!flag) //Announces that the username didn't enter correct data
            {
                MessageBox.Show("Wrong username or password!!");
            }
            else
            {
                frmBithdaysCalendar f = new frmBithdaysCalendar(_userName);
                this.Hide();
                f.ShowDialog();
                this.Close();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(100);
        }

    }
}
