﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace HW19_LiroyBarzillai
{
    public partial class frmBithdaysCalendar : Form
    {
        private Dictionary<string, string> _data = new Dictionary<string, string>();

        public frmBithdaysCalendar(string username)
        {
            string fileName = username + "BD.txt";
            string[] data = null;
            string[] lines = null;

            try  //Checks if we can open the file
            {
                lines = System.IO.File.ReadAllLines(fileName);  //Gets all the data from the file
                for (int i = 0; i < lines.Length; i++)  //Inserts the data into the dictionary
                {
                    data = lines[i].Split(',');
                    _data.Add(data[1], data[0]);
                }
            }
            catch (Exception ex)
            {
                FileStream fs = File.Create(fileName);   //Creates a file
                fs.Close();
            }

            InitializeComponent();
        }

        private void frmBithdaysCalendar_Load(object sender, EventArgs e)
        {
            
        }

        private void calendarBirthdays_DateChanged(object sender, DateRangeEventArgs e)
        {

            if (calendarBirthdays.SelectionStart.Date != DateTime.MinValue)  //Checks if the username has selected a date
            {
                DateTime date = calendarBirthdays.SelectionStart;  //Gets the selected date
                int day = date.Day;
                int month = date.Month;
                int year = date.Year;
                string finalDate = month.ToString() + '/' + day.ToString() + '/' + year.ToString(); //Creates a string from the date

                if (_data != null && _data.ContainsKey(finalDate))  //Checks if someone celebrates birthday on this day
                {
                    lblCelebrator.Text = "!חוגג יום הולדת בתאריך הנבחר " + _data[finalDate];
                }
                else
                {
                    lblCelebrator.Text = "!בתאריך הנבחר – אף אחד לא חוגג יום הולדת";
                }
            }
        }
    }
}
