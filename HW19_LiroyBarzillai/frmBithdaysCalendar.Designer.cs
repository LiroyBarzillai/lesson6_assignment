﻿namespace HW19_LiroyBarzillai
{
    partial class frmBithdaysCalendar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.calendarBirthdays = new System.Windows.Forms.MonthCalendar();
            this.lblCelebrator = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // calendarBirthdays
            // 
            this.calendarBirthdays.Location = new System.Drawing.Point(28, 18);
            this.calendarBirthdays.Name = "calendarBirthdays";
            this.calendarBirthdays.TabIndex = 0;
            this.calendarBirthdays.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.calendarBirthdays_DateChanged);
            // 
            // lblCelebrator
            // 
            this.lblCelebrator.AutoSize = true;
            this.lblCelebrator.Font = new System.Drawing.Font("Oswald", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCelebrator.Location = new System.Drawing.Point(20, 189);
            this.lblCelebrator.Name = "lblCelebrator";
            this.lblCelebrator.Size = new System.Drawing.Size(0, 20);
            this.lblCelebrator.TabIndex = 1;
            // 
            // frmBithdaysCalendar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 215);
            this.Controls.Add(this.lblCelebrator);
            this.Controls.Add(this.calendarBirthdays);
            this.Name = "frmBithdaysCalendar";
            this.Text = "ימי הולדת";
            this.Load += new System.EventHandler(this.frmBithdaysCalendar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar calendarBirthdays;
        private System.Windows.Forms.Label lblCelebrator;
    }
}